# Copyright 2014 Michael Forney
# Distributed under the terms of the GNU General Public License v2

require github [ user=michaelforney ]

SUMMARY="swc is a library for making a simple Wayland compositor"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    libinput  [[ description = [ Support input device handling through libinput ] ]]
    xwayland  [[ description = [ Support for X11 applications through Xwayland ] ]]
    libinput? ( ( providers: eudev systemd ) [[ number-selected = exactly-one ]] )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/wld[drm]
        sys-libs/wayland
        x11-dri/libdrm
        x11-libs/libevdev
        x11-libs/libxkbcommon
        x11-libs/pixman
        libinput? (
            sys-libs/libinput
            providers:eudev? ( sys-apps/eudev )
            providers:systemd? ( sys-apps/systemd )
        )
        xwayland? (
            x11-libs/libxcb
            x11-utils/xcb-util-wm
        )
    run:
        xwayland? ( x11-server/xorg-server[xwayland] )
"

BUGS_TO="mforney@mforney.org"

DEFAULT_SRC_COMPILE_PARAMS=( V=1 )

src_configure() {
    cat > config.mk << EOF
PREFIX              = /usr/$(exhost --target)
LIBDIR              = /usr/$(exhost --target)/lib
DATADIR             = /usr/share
ENABLE_STATIC       = 1
ENABLE_SHARED       = 1
ENABLE_LIBINPUT     = $(option libinput 1 0)
ENABLE_XWAYLAND     = $(option xwayland 1 0)
EOF

    cat config.mk
}

